.. |label| replace:: 1st Vision Erlaube Sonderzeichen in Artikelnummern
.. |snippet| replace:: FvSpecialCharsInArticleNumber
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.3.0
.. |maxVersion| replace:: 5.3.4
.. |version| replace:: 1.0.0
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Shopware erlaubt bei der Eingabe von Artikelnummern nur alphanumerische Zeichen a-Z 0-9 und die Zeichen "." "-" "_".
Mit diesen Plugin können Sie grundsätzlich alle latin-1 characters setzen.
Im Shop sind weiterhin alle Funktionen der Shopware Standardinstallation kompatibel mit den Sonderzeichen – auch die Shop-Suche.
Kompatibilität mit Premium Plugins oder Plugins von Drittanbietern wurde nicht getestet und kann nicht garantiert werden.

Frontend
--------
keine

Backend
-------
keine

technische Beschreibung
------------------------
keine

Modifizierte Template-Dateien
-----------------------------
:/index/index.tpl:



